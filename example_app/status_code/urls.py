from django.urls import path
from . import views

urlpatterns = [
    path(
        'successful/',
        views.successful,
        name='successful'
    ),
    path(
        'client/',
        views.client,
        name='client'
    )
]
