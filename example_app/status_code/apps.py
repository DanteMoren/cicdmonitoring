from django.apps import AppConfig


class StatusCodeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'status_code'
