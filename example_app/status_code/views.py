from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status


@api_view(['GET'])
def successful(request):
    return Response(
        {
            'message': 'Запрос успешен.'
        }, status=status.HTTP_200_OK
    )


@api_view(['GET'])
def client(request):
    return Response(
        {
            'errors': 'Ошибка на стороне клиента.'
        }, status=status.HTTP_409_CONFLICT
    )
